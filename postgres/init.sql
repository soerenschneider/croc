CREATE TABLE IF NOT EXISTS products (
    ean BIGINT PRIMARY KEY,
    name TEXT,
    detailed_name TEXT,
    manufacturer TEXT,
    category TEXT,
    subcategory TEXT
);

CREATE TABLE IF NOT EXISTS items (
    ean BIGINT PRIMARY KEY REFERENCES products(ean),
    preselected BOOLEAN,
    quantity SMALLINT,
    date DATE
);

DROP USER IF EXISTS grafanareader;
CREATE USER grafanareader WITH PASSWORD 'pass';
GRANT SELECT ON item TO grafanareader;
