from products.products import Products
from items.items import Items
from items.item import Item

import datetime

class Facade:
    def __init__(self):
        self.products = Products()
        self.items = Items()

    def add_item(self, request):
        product = self.products.get(request['ean'])
        item = self.items.get(request['ean'])

        if None is item:
            item = Item(request['ean'])
            item.date = datetime.datetime.now()
        else:
            item.quantity += 1

        self.items.add(item)
        return item

    def rem_item(self, request):
        item = self.items.get(request['ean'])
        if None is item:
            return -1

        quantity = 1
        try:
            quantity = request['quantity']
        except KeyError:
            pass

        item.quantity -= quantity
        if item.quantity < 1:
            return self.items.delete(item.ean)

        return self.items.update(item)