FROM python:3 as python-base
COPY requirements.txt .
RUN pip install -r requirements.txt

FROM python:3.7-slim

COPY requirements.txt /opt/croc/requirements.txt
WORKDIR /opt/croc
COPY --from=python-base /root/.cache /root/.cache
RUN pip install -r requirements.txt && rm -rf /root/.cache

COPY main.py facade.py products items /opt/croc/

RUN useradd toor
USER toor

CMD ["python3", "main.py"]
