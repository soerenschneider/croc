from bs4 import BeautifulSoup
import requests

from products.eanproviders.eanprovider import EanProvider
from products.product import Product
import products.errors


class OpenGtinDbProvider(EanProvider):
    @staticmethod
    def _fetch_site(ean):
        url = f"https://opengtindb.org/index.php?cmd=ean1&ean={ean}&sq=1"
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41',
        }
        response = requests.get(url, headers=headers)
        return response.text

    @staticmethod
    def _parse(text):
        data = []
        soup = BeautifulSoup(text, "lxml")
        table = soup.findAll('table')[1]

        rows = table.find_all('tr')
        for row in rows:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols if ele]
            b = [ele.replace("\n", " ") for ele in cols if ele]
            if len(b) > 1:
                data.append(b)

        return data

    @staticmethod
    def _convert(data, ean):
        item = Product()
        item.ean = ean
        for row in data:
            key, val = row[0], row[1]
            if "Hauptkategorie:" == key:
                item.category = val
            elif "Unterkategorie:" == key:
                item.subcategory = val
            elif "Name:" == key:
                item.name = val
            elif "Detailname:" == key:
                item.detailed_name = val
            elif "Hersteller:" == key:
                item.manufacturer = val

        return item

    def _inspect_text(self, text, ean):
        if "Fehler: Diese EAN/GTIN ist derzeit noch nicht bekannt." in text:
            raise products.errors.ProductNotFoundError(f"Product not found: {ean}")
        if "Fehler: Ung&uuml;ltige EAN/GTIN eingegeben" in text:
            raise products.errors.InvalidEanError(f"EAN {ean} is not considered valid")

    def fetch_ean(self, ean):
        text = self._fetch_site(ean)
        self._inspect_text(text, ean)
        raw_item = self._parse(text)
        converted = self._convert(raw_item, ean)
        return converted
