from dataclasses import dataclass


@dataclass(init=False)
class Product:
    ean: int = 0
    name: str = ""
    detailed_name: str = ""
    manufacturer: str = ""
    category: str = ""
    subcategory: str = ""

    @staticmethod
    def from_dict(d):
        if d is None:
            return None

        product = Product()

        product.ean = d.get('ean')
        product.name = d.get('name')
        product.detailed_name = d.get('detailed_name')
        product.category = d.get('category')
        product.subcategory = d.get('subcategory')
        product.manufacturer = d.get('manufacturer')

        return product

    def to_dict(self):
        d = dict()

        d['ean'] = self.ean
        d['name'] = self.name
        d['detailed_name'] = self.detailed_name
        d['category'] = self.category
        d['subcategory'] = self.subcategory
        d['manufacturer'] = self.manufacturer

        return d
