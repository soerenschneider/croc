
from products.db import ProductDb
from products.fetch import EanProductProvider
import logging


class Products:
    def __init__(self):
        self.db = ProductDb("localhost", "groc", "groc")
        self.resolver = EanProductProvider()
        self.logger = logging.getLogger('items')

    def get(self, ean):
        item = self.db.get_product(ean)
        if item is None:
            self.logger.warning(f"Item '{ean}'' not found in DB, looking it up..")
            item = self.resolver.fetch_ean(ean)

            if item is None:
                self.logger.info(f"No results found for '{ean}'..")
            else:
                self.logger.info(f"Found '{item.name}' for '{ean}'")
                self.db.add_product(item)

        return item

    def delete(self, ean):
        return self.db.delete_product(ean)
