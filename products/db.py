import psycopg2
import psycopg2.extras

from products.product import Product


class ProductDb:
    def __init__(self, host, user, pw, dbname = "groc"):
        self.host = host
        self.user = user
        self.pw = pw
        self.dbname = dbname
    
    def get_product(self, ean):
        with psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.dbname) as db:
            with db.cursor(cursor_factory=psycopg2.extras.DictCursor) as cursor:
                sql = f"SELECT * FROM products WHERE EAN = %s"
                cursor.execute(sql, (ean,))
                fetched = cursor.fetchone()
                return Product.from_dict(fetched)

    def add_product(self, product):
        with psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.dbname) as db:
            with db.cursor() as cursor:
                sql = f"INSERT INTO products (ean, name, detailed_name, manufacturer, category, subcategory) VALUES(%s, %s, %s, %s, %s, %s) ON CONFLICT (ean) DO UPDATE SET name = %s, detailed_name = %s, manufacturer = %s, category = %s, subcategory = %s WHERE products.ean = %s"
                cursor.execute(sql, (
                    product.ean,
                    product.name,
                    product.detailed_name,
                    product.manufacturer,
                    product.category,
                    product.subcategory,
                    product.name,
                    product.detailed_name,
                    product.manufacturer,
                    product.category,
                    product.subcategory,
                    product.ean)
                            )
                db.commit()

    def delete_product(self, ean):
        with psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.dbname) as db:
            with db.cursor() as cursor:
                sql = f"DELETE FROM products WHERE ean = %s"
                cursor.execute(sql, (ean,))
                rows_deleted = cursor.rowcount
                db.commit()
                return rows_deleted
