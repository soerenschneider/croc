
from products.eanproviders.eanprovider import EanProvider
from products.eanproviders.opengtin import OpenGtinDbProvider


class EanProductProvider(EanProvider):
    def __init__(self, provider=OpenGtinDbProvider()):
        self.provider = provider

    def fetch_ean(self, ean):
        return self.provider.fetch_ean(ean)
