import psycopg2
import psycopg2.extras

from items.item import Item


class ItemDb:
    __tablename = "items"

    def __init__(self, host, user, pw, dbname = "groc"):
        self.host = host
        self.user = user
        self.pw = pw
        self.dbname = dbname

    def get_items(self):
        with psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.dbname) as db:
            with db.cursor(cursor_factory=psycopg2.extras.DictCursor) as cursor:
                sql = f"SELECT * FROM {self.__tablename}"
                cursor.execute(sql)
                return [Item.from_dict(x) for x in cursor.fetchall()]

    def get_item(self, ean):
        with psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.dbname) as db:
            with db.cursor(cursor_factory=psycopg2.extras.DictCursor) as cursor:
                sql = f"SELECT * FROM {self.__tablename} WHERE ean = %s"
                cursor.execute(sql, (ean,))
                fetched = cursor.fetchone()
                return Item.from_dict(fetched)

    def add_item(self, product):
        with psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.dbname) as db:
            with db.cursor() as cursor:
                sql = f"INSERT INTO {self.__tablename} (ean, preselected, quantity, date) VALUES(%s, %s, %s, %s) ON CONFLICT (ean) DO UPDATE SET preselected = %s, quantity = %s, date = %s WHERE {self.__tablename}.ean = %s"
                cursor.execute(sql, (
                    product.ean,
                    product.preselected,
                    product.quantity,
                    product.date,
                    product.preselected,
                    product.quantity,
                    product.date,
                    product.ean)
                    )
                rows_modified = cursor.rowcount
                db.commit()
                return rows_modified

    def update_item(self, product):
        with psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.dbname) as db:
            with db.cursor() as cursor:
                sql = f"UPDATE {self.__tablename} SET preselected = %s, quantity = %s, date = %s WHERE {self.__tablename}.ean = %s"
                cursor.execute(sql, (
                    product.preselected,
                    product.quantity,
                    product.date,
                    product.ean)
                               )
                rows_modified = cursor.rowcount
                db.commit()
                return rows_modified

    def delete_item(self, ean):
        with psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.dbname) as db:
            with db.cursor() as cursor:
                sql = f"DELETE FROM {self.__tablename} WHERE id = %s"
                cursor.execute(sql, (ean,))
                rows_deleted = cursor.rowcount
                db.commit()
                return rows_deleted

    def delete_items(self):
        with psycopg2.connect(host=self.host, user=self.user, password=self.pw, dbname=self.dbname) as db:
            with db.cursor() as cursor:
                sql = f"DELETE FROM {self.__tablename}"
                cursor.execute(sql)
                rows_deleted = cursor.rowcount
                db.commit()
                return rows_deleted
