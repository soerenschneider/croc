from dataclasses import dataclass
import datetime


@dataclass
class Item:
    ean: int
    preselected: bool = False
    quantity: int = 1
    date: datetime.datetime = None

    @staticmethod
    def from_dict(d):
        if d is None:
            return None

        item = Item(d.get('ean'))

        item.quantity = d.get('quantity')
        item.preselected = d.get('preselected')
        item.date = d.get('date')

        return item

    def to_dict(self):
        d = dict()

        d['ean'] = self.ean

        d['preselected'] = self.preselected
        d['quantity'] = self.quantity
        d['date'] = self.date

        return d
