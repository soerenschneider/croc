
from items.db import ItemDb
from items.item import Item

import datetime

import logging


class Items:
    def __init__(self):
        self.db = ItemDb("localhost", "groc", "groc")
        self.logger = logging.getLogger('items')

    def get_all(self):
        return self.db.get_items()

    def get(self, ean):
        return self.db.get_item(ean)

    def add(self, item):
        if not item.date:
            item.date = datetime.datetime.now()
        return self.db.add_item(item)

    def delete(self, ean):
        return self.db.delete_item(ean)

    def delete_all(self):
        return self.db.delete_items()