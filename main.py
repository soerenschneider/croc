from products.products import Products
from items.items import Items

from flask import Flask
from flask import request
from flask import Response

from datetime import date
from datetime import datetime

from facade import Facade

import products.errors

import json



app = Flask(__name__)

a = Products()
i = Items()
facade = Facade()


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, date):
            return datetime.strftime(o, '%Y-%m-%d')

        return json.JSONEncoder.default(self, o)

@app.route("/products", methods=['GET'])
def get_products(ean):
    data = json.dumps(a.get(ean).to_dict(), ensure_ascii=False)
    return Response(data, status=200, mimetype="application/json")

@app.route("/products/<ean>", methods=['GET'])
def get_product(ean):
    data = json.dumps(a.get(ean).to_dict(), ensure_ascii=False)
    return Response(data, status=200, mimetype="application/json")


@app.route("/products/<ean>", methods=['DELETE'])
def delete(ean):
    rows = a.delete(ean)
    data = json.dumps('{{"deleted_rows":{rows}}}')
    return Response(data, status=200, mimetype="application/json")

@app.route("/items", methods=['POST'])
def new_item():
    if not request.is_json:
        return Response("Please send json", status=416)

    item = facade.add_item(request.get_json())
    jitem = json.dumps(item.to_dict(), ensure_ascii=False, cls=DateTimeEncoder)
    return Response(jitem, status=200, mimetype="application/json")


@app.route("/items")
def list():
    items = i.get_all()
    mappy = [x.to_dict() for x in items]
    return json.dumps(mappy, ensure_ascii=False, cls=DateTimeEncoder)


@app.route("/items", methods=['DELETE'])
def delete_items():
    if not request.is_json:
        return Response("Please send json", status=416)

    facade.rem_item(request.json)
    return Response("ok", status=200)


@app.errorhandler(products.errors.ProductNotFoundError)
def handle_bad_request(e):
    return repr(e), 400


@app.errorhandler(products.errors.InvalidEanError)
def handle_bad_request(e):
    return repr(e), 400


if __name__ == '__main__':
    app.run(host='0.0.0.0')
